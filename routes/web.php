<?php

use App\Http\Controllers\CategoryClientController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProfileController;
use App\Models\CategoriesClients;
use App\Models\Category;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');

})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/products', [ProductController::class,'all'])->name('products.all');

Route::get('product/{product}/deleteConfirm', [ProductController::class,'deleteConfirm'])->name('products.deleteconfirm');

Route::get('product/{product}/delete', [ProductController::class,'delete'])->name('products.delete');

Route::get('/products/form', function () {
    $categories = Category::all();
    return view('/products/productform', compact('categories'));
})->name('products.form');


Route::post('/product/insert', [ProductController::class,'insert'])->name('products.insert');

// Clientes

Route::get('/clients', [ClientController::class,'allClients'])->name('clients.all');

Route::get('client/{client}/deleteClientConfirm', [ClientController::class,'deleteClientConfirm'])->name('clients.deleteconfirm');

Route::get('client/{client}/deleteClient', [ClientController::class,'deleteClient'])->name('clients.delete');

Route::get('/clients/form', function () {
    $categoriesClients = CategoriesClients::all();
    return view('/clients/clientform', compact('categoriesClients'));
})->name('clients.form');

Route::post('/client/insert', [ClientController::class,'insertClient'])->name('clients.insert');

// Orders

Route::get('/orders', [OrderController::class, 'all'])->name('orders.all');

Route::get('/orders/create', [OrderController::class, 'create'])->name('orders.create');
Route::post('/orders', [OrderController::class, 'store'])->name('orders.store');

// Categories

Route::get('/categories', [CategoryController::class, 'all'])->name('categories.all');

Route::get('/category/create', function (){
    return view('/category/createCategory');
})->name('category.form');

Route::get('/categories/edit/{category}', [CategoryController::class, 'edit'])->name('categories.edit');

Route::post('/category/insert', [CategoryController::class,'create'])->name('category.insert');

Route::post('/categories/update/{category}', [CategoryController::class, 'update'])->name('categories.update');


Route::get('category/{category}/deleteConfirm', [CategoryController::class,'deleteConfirmCategory'])->name('category.deleteconfirm');

Route::get('category/{category}/deleteCategory', [CategoryController::class,'deleteCategory'])->name('category.delete');

// Categories Clients

Route::get('/categoriesClients', [CategoryClientController::class, 'all'])->name('categoriesclient.all');

Route::get('/categoryClient/create', function (){
    return view('/categoryClient/createCategoryClient');
})->name('categoryClient.form');

Route::get('/categoriesClients/edit/{categoryClient}', [CategoryClientController::class, 'edit'])->name('categoriesclient.edit');

Route::post('/categoryClient/insert', [CategoryClientController::class,'create'])->name('categoryClient.insert');

Route::post('/categoriesClients/update/{categoryClient}', [CategoryClientController::class, 'update'])->name('categoriesclient.update');


Route::get('categoryClient/{categoryClient}/deleteConfirm', [CategoryClientController::class,'deleteConfirmCategory'])->name('categoryClient.deleteconfirm');

Route::get('categoryClient/{categoryClient}/deleteCategory', [CategoryClientController::class,'deleteCategory'])->name('categoryClient.delete');
require __DIR__.'/auth.php';
