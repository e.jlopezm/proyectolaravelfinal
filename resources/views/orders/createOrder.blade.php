<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mi página de prueba</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Crear Pedido') }}
        </h2>
    </x-slot>

    <div class="py-12 d-flex justify-content-center">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="card shadow-sm">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{ route('orders.store') }}">
                        @csrf
                        <div class="mb-3">
                            <label for="client_id" class="form-label">Cliente</label>
                            <select name="client_id" id="client_id" class="form-select" required>
                                <option value="">Seleccione un cliente</option>
                                @foreach($clients as $client)
                                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="order_date" class="form-label">Fecha del Pedido</label>
                            <input type="date" name="order_date" id="order_date" class="form-control" required>
                        </div>
                        <div id="products-container">
                            <div class="product-entry mb-3">
                                <label for="products[0][id]" class="form-label">Producto</label>
                                <select name="products[0][id]" id="products[0][id]" class="form-select" required>
                                    <option value="">Seleccione un producto</option>
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                                    @endforeach
                                </select>
                                <label for="products[0][quantity]" class="form-label mt-2">Cantidad</label>
                                <input type="number" name="products[0][quantity]" id="products[0][quantity]" class="form-control" min="1" required >
                                @if(session('quantity'))
                                    <div class="alert alert-danger mt-3">
                                        {{ session('quantity') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="d-flex justify-content-center mt-4">
                            <button type="button" id="add-product" class="btn btn-secondary me-2">Añadir Producto</button>
                            <button type="submit" class="btn btn-primary">Hacer un Pedido</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXlY3/3vzpFfgccz7zU3YLQjSxsb6N0IFB+z7rrn3A88mTAA8bT8b4O2L8w4" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        const addProductButton = document.getElementById('add-product');
        const productsContainer = document.getElementById('products-container');
        let productIndex = 1;

        addProductButton.addEventListener('click', function() {
            const productEntry = document.createElement('div');
            productEntry.className = 'product-entry mb-3';

            productEntry.innerHTML = `
                <label class="form-label" for="products[${productIndex}][id]">Producto</label>
                <select name="products[${productIndex}][id]" class="form-select">
                    <option value="">Seleccione un producto</option>
                    @foreach($products as $product)
            <option value="{{ $product->id }}">{{ $product->name }} </option>
                    @endforeach
            </select>
            <label class="form-label mt-2" for="products[${productIndex}][quantity]">Cantidad</label>
                <input type="number" name="products[${productIndex}][quantity]" class="form-control" placeholder="Cantidad" min="1" required>
            `;

            productsContainer.appendChild(productEntry);
            productIndex++;
        });
    });
</script>
</body>
</html>
