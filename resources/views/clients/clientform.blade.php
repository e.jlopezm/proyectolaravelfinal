<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mi página de prueba</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <div class="d-flex justify-content-between align-items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Crear Cliente') }}
            </h2>
        </div>
    </x-slot>
    <div class="container py-3 bg-white rounded mt-4">
        <form action="{{ route('clients.insert') }}" method="post">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Nombre</label>
                <input type="text" class="form-control" name="name" id="name" value="{{old("name")}}">
                @error('name')
                <div class="alert alert-danger mt-2">
                    Error en el nombre, tiene más de 100 caracteres o ya existe
                </div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="surname" class="form-label">Apellido</label>
                <input type="text" class="form-control" name="surname" id="surname" value="{{old("surname")}}">
                @error('surname')
                <div class="alert alert-danger mt-2">
                    Error en el apellido, tiene más de 100 caracteres
                </div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="text" class="form-control" name="email" id="email" value="{{old("email")}}">
                @error('email')
                <div class="alert alert-danger mt-2">
                    Error en el email, este campo es obligatorio, ya está registrado o tiene más de 100 caracteres
                </div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="phonenumber" class="form-label">Numero de telefono</label>
                <input type="text" class="form-control" name="phonenumber" id="phonenumber" value="{{old("phonenumber")}}">
                @error('phonenumber')
                <div class="alert alert-danger mt-2">
                    Error en el numero de telefono, este campo es obligatorio, ya está registrado o tiene más de 100 caracteres
                </div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="postalcode" class="form-label">Codigo postal</label>
                <input type="text" class="form-control" name="postalcode" id="postalcode" value="{{old("postalcode")}}">
                @error('postalcode')
                <div class="alert alert-danger mt-2">
                    Error en el codigo postal, este campo es obligatorio o tiene más de 20 caracteres
                </div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="categories_clients_id" class="form-label">Categoria</label>
                <select name="categories_clients_id" id="categories_clients_id" class="form-select" required>
                    <option value="">Seleccione una categoria</option>
                    @foreach($categoriesClients as $categoriesClient)
                        <option value="{{ $categoriesClient->id }}">{{ $categoriesClient->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label for="address" class="form-label">Dirección</label>
                <input type="text" class="form-control" name="address" id="address" value="{{old("address")}}">
                @error('address')
                <div class="alert alert-danger mt-2">
                    Error en la dirección, este campo es obligatorio o tiene más de 100 caracteres
                </div>
                @enderror
            </div>


            <div class="mb-3">
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
        </form>
    </div>
</x-app-layout>
<script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXlY3/3vzpFfgccz7zU3YLQjSxsb6N0IFB+z7rrn3A88mTAA8bT8b4O2L8w4" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhG8c3AnjE7e3hf9gzUqvQhtP6HfhZfp26d5URjo8U2tE3JfCFk3PB6h59ni" crossorigin="anonymous"></script>
</body>
</html>

