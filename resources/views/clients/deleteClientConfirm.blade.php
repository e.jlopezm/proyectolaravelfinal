<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mi página de prueba</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<div class="container mt-5">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Confirmación de eliminación</h5>
            <p class="card-text">¿Estás seguro que quieres borrar el cliente?</p>
            <div class="d-flex justify-content-end">
                <a href="{{route('clients.delete',['client' => $client->id])}}" class="btn btn-danger me-2">
                    Sí
                </a>
                <a href="{{route('clients.all')}}" class="btn btn-secondary">
                    No
                </a>
            </div>
        </div>
    </div>
</div>

<script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXlY3/3vzpFfgccz7zU3YLQjSxsb6N0IFB+z7rrn3A88mTAA8bT8b4O2L8w4" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhG8c3AnjE7e3hf9gzUqvQhtP6HfhZfp26d5URjo8U2tE3JfCFk3PB6h59ni" crossorigin="anonymous"></script>
</body>
</html>
