<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class clients extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert(['name'=>'Pedro', 'surname'=>'pedro', 'email'=>'pedro@gmail.com','postalcode'=>'08219', 'phonenumber' =>'8283716', 'address'=>'Calle manolo', 'categories_clients_id' => 1, 'categories_clients_name' => 'VIP']);
        DB::table('clients')->insert(['name'=>'Pedro2', 'surname'=>'pedro2', 'email'=>'pedro2@gmail.com','postalcode'=>'08219', 'phonenumber' =>'8283716', 'address'=>'Calle manolo' , 'categories_clients_id' => 2, 'categories_clients_name' => 'Premium+']);
        DB::table('clients')->insert(['name'=>'Pedro3', 'surname'=>'pedro3', 'email'=>'pedro3@gmail.com','postalcode'=>'08219', 'phonenumber' =>'8283716', 'address'=>'Calle manolo','categories_clients_id' => 3, 'categories_clients_name' => 'Premium']);
        DB::table('clients')->insert(['name'=>'Pedro4', 'surname'=>'pedro4', 'email'=>'pedro4@gmail.com','postalcode'=>'08219', 'phonenumber' =>'8283716', 'address'=>'Calle manolo','categories_clients_id' => 4, 'categories_clients_name' => 'Estandar+']);
        DB::table('clients')->insert(['name'=>'Pedro5', 'surname'=>'pedro5', 'email'=>'pedro5@gmail.com','postalcode'=>'08219', 'phonenumber' =>'8283716', 'address'=>'Calle manolo', 'categories_clients_id' => 5, 'categories_clients_name' => 'Estandar']);

    }
}
