<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class category extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert(['name'=>'Bebida', 'iva'=>21]);
        DB::table('categories')->insert(['name'=>'Carne', 'iva'=>5]);
        DB::table('categories')->insert(['name'=>'Pescado', 'iva'=>5]);
        DB::table('categories')->insert(['name'=>'Utensilios', 'iva'=>13.5]);
        DB::table('categories')->insert(['name'=>'Limpieza', 'iva'=>21]);



    }
}
