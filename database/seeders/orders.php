<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class orders extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('orders')->insert(['client_id'=>1, 'custom_id'=>"2023/1", 'order_date'=>'2024-05-30', 'total_price'=>33,'total_price_with_iva'=>39.93]);
        DB::table('order_product')->insert(['order_id'=>1, 'product_id'=>1, 'quantity'=>5,'price'=>20]);
        DB::table('orders')->insert(['client_id'=>2, 'custom_id'=>"2023/2", 'order_date'=>'2024-05-30', 'total_price'=>33,'total_price_with_iva'=>39.93]);
        DB::table('order_product')->insert(['order_id'=>2, 'product_id'=>2, 'quantity'=>5,'price'=>20]);
        DB::table('orders')->insert(['client_id'=>3,'custom_id'=>"2023/3", 'order_date'=>'2024-05-30', 'total_price'=>33,'total_price_with_iva'=>39.93]);
        DB::table('order_product')->insert(['order_id'=>3, 'product_id'=>3, 'quantity'=>5,'price'=>20]);
        DB::table('orders')->insert(['client_id'=>4,'custom_id'=>"2023/4", 'order_date'=>'2024-05-30', 'total_price'=>33,'total_price_with_iva'=>39.93]);
        DB::table('order_product')->insert(['order_id'=>4, 'product_id'=>4, 'quantity'=>5,'price'=>20]);
        DB::table('orders')->insert(['client_id'=>5,'custom_id'=>"2023/5", 'order_date'=>'2024-05-30', 'total_price'=>33,'total_price_with_iva'=>39.93]);
        DB::table('order_product')->insert(['order_id'=>5, 'product_id'=>5, 'quantity'=>5,'price'=>20]);

    }
}
