<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class categoryClient extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories_clients')->insert(['name'=>'VIP', 'descuento'=>50]);
        DB::table('categories_clients')->insert(['name'=>'Premium+', 'descuento'=>40]);
        DB::table('categories_clients')->insert(['name'=>'Premium', 'descuento'=>30]);
        DB::table('categories_clients')->insert(['name'=>'Estandar+', 'descuento'=>15]);
        DB::table('categories_clients')->insert(['name'=>'Estandar', 'descuento'=>0]);
    }
}
