<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class products extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert(['name'=>'Test', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC', 'category_id'=>1, 'category_name'=>"Bebida"]);
        DB::table('products')->insert(['name'=>'Test1', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC1','category_id'=>2, 'category_name'=>"Carne"]);
        DB::table('products')->insert(['name'=>'Test2', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC2','category_id'=>3, 'category_name'=>"Pescado"]);
        DB::table('products')->insert(['name'=>'Test3', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC3','category_id'=>4, 'category_name'=>"Utensilios"]);
        DB::table('products')->insert(['name'=>'Test4', 'quantity'=>1, 'price'=>33,'description'=>'TEST DESC4','category_id'=>5, 'category_name'=>"Limpieza"]);
    }
}
