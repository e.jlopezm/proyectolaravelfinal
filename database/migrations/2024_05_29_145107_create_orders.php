<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            //$table->biginteger('client_id')->unsigned();
            $table->foreignId('client_id')->constrained()->onDelete('cascade');
            $table->string('custom_id')->nullable()->unique();
            $table->date('order_date');
            $table->decimal('total_price', 8, 2)->default(0);
            $table->decimal('total_price_with_iva', 8, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
