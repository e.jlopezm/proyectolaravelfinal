<?php

namespace App\Http\Controllers;

use App\Models\CategoriesClients;
use App\Models\Category;
use App\Models\Client;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function all()
    {
        $orders = Order::with('client','products')->get();
        return view('orders.getorders', compact('orders'));

    }
    public function create()
    {
        $clients = Client::all();
        $products = Product::all();
        return view('orders.createOrder', compact('clients', 'products'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'client_id' => 'required|exists:clients,id',
            'order_date' => 'required|date',
            'products.*.id' => 'required|exists:products,id',
            'products.*.quantity' => 'required|integer|min:1',
        ]);

        // he intentado rellenar la customid de esta forma, pero me lo deja vacio
        $orderDate = $validated['order_date'];
        $year = date('Y', strtotime($orderDate));

        $customId = Order::generateCustomId($year);

        foreach ($request->products as $product) {

            $productModel = Product::find($product['id']);
            $quantity = $product['quantity'];
            if (($productModel->quantity - $quantity) < 0) {
                return redirect()->back()->with('quantity', 'La cantidad solicitada para ' . $productModel->name . ' excede el stock disponible.');
            }
        }

        // Crear la orden
        $order = Order::create([
            'client_id' => $request->client_id,
            'custom_id' => $customId,
            'order_date' => $request->order_date,
        ]);

        $order->save();

        $totalWithIva = 0;
        $totalWithoutIva = 0;


        $clientModel = Client::find($request->client_id);
        $descuentoId = $clientModel->categories_clients_id;
        $categories_clients = CategoriesClients::find($descuentoId);
        $descuento = $categories_clients->descuento;



        foreach ($request->products as $product) {

            $productModel = Product::find($product['id']);
            $category = Category::find($productModel->category_id);
            $quantity = $product['quantity'];

            $price = $productModel->price;
            //$category = $productModel->category;
            $ivaPercentage = $category->iva;

            $descuentoValor = ($price * $quantity) * ($descuento/100);
            $totalProductWithoutIva = ($price * $quantity) - $descuentoValor;
            $totalWithoutIva += $totalProductWithoutIva;
            //$totalWithoutIva = $price;
            $totalProductWithIva = ($price * $quantity * (1 + ($ivaPercentage / 100))) - $descuentoValor;
            $totalWithIva += $totalProductWithIva;

            $order->products()->attach($product['id'], [
                'quantity' => $quantity,
                'price' => $price,
            ]);

            $productModel->quantity -= $quantity;
            $productModel->save();

            $order->total_price = $totalWithoutIva;
            $order->total_price_with_iva = $totalWithIva;
            $order->save();
        }

        return redirect()->route('orders.all')->with('success', 'Orden creada exitosamente');
    }
}
