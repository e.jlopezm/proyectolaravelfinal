<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Client;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    public function all()
    {
        $categories = Category::all();
        return view('category/getcategories')->with('categories', $categories);

    }
    public function deleteCategory(Category $category):RedirectResponse{
        $category->delete();
        return Redirect::route('categories.all');
    }
    public function deleteConfirmCategory(Category $category){
        return view('/category/deleteCategoryConfirm')->with('category', $category);
    }
    public function create(Request $request):RedirectResponse
    {
        $validated = $request->validate([
            'name' => 'required|unique:categories|max:100',
            'iva' => 'required'
        ]);

        DB::table('categories')->insert(['name' =>$validated['name'], 'iva'=>$validated['iva']]);
        return Redirect::route('categories.all');
    }
    public function edit(Category $category)
    {
        return view('/category/editCategory', compact('category'));
    }

    public function update(Request $request, Category $category): RedirectResponse
    {
        $validated = $request->validate([
            'name' => 'required|max:100|unique:categories,name,' . $category->id,
            'iva' => 'required|numeric|min:0',
        ]);

        Product::where('category_id', $category->id)->update(['category_name' => $validated['name']]);


        $category->update($validated);
        return Redirect::route('categories.all');
    }
}
