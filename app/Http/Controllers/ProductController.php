<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProductController extends Controller
{
    public function all()
    {
        $products = Product::all();
        return view('/products/getproducts')->with('products', $products);
    }
    public function delete(Product $product):RedirectResponse{
        $product->delete();
        return Redirect::route('products.all');
    }
    public function deleteConfirm(Product $product){
        return view('/products/deleteConfirm')->with('product', $product);
    }
    public function insert(Request $request):RedirectResponse{


        $validated = $request->validate([
            'name' => 'required|unique:products|max:100',
            'quantity' => 'required',
            'price' => 'required',
            'description' => 'max:1000',
            'category_id' => 'required|exists:categories,id'
        ]);


            $categoryModel = Category::find($request->category_id);
            $name = $categoryModel->name;


        DB::table('products')->insert(['name' =>$validated['name'], 'quantity'=>$validated['quantity'], 'price' =>$validated['price'],
            'description' =>$validated['description']."", 'category_id' =>$validated['category_id'], 'category_name' => $name]);
        return Redirect::route('products.all');
    }

}
