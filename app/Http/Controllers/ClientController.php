<?php

namespace App\Http\Controllers;

use App\Models\CategoriesClients;
use App\Models\Category;
use App\Models\Client;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ClientController extends Controller
{
    public function allClients()
    {
        $clients = Client::all();
        return view('/clients/getclients')->with('clients', $clients);
    }
    public function deleteClient(Client $client):RedirectResponse{
        $client->delete();
        return Redirect::route('clients.all');
    }
    public function deleteClientConfirm(Client $client){
        return view('/clients/deleteClientConfirm')->with('client', $client);
    }
    public function insertClient(Request $request){

        $validated = $request->validate([
            'name' => 'required|unique:clients|max:100',
            'surname' => 'max:100',
            'email' => 'required|unique:clients|max:100',
            'phonenumber' => 'required|unique:clients|max:100',
            'postalcode' => 'required|max:20',
            'categories_clients_id' => 'required|exists:categories_clients,id',
            'address' => 'required|max:100'
        ]);

        $categoryModel = CategoriesClients::find($request->categories_clients_id);
        $name = $categoryModel->name;



        DB::table('clients')->insert(['name' =>$validated['name'], 'surname'=>$validated['surname'], 'email' =>$validated['email'],
            'phonenumber' =>$validated['phonenumber'], 'postalcode' =>$validated['postalcode'], 'address'=>$validated['address'],
            'categories_clients_id'=>$validated['categories_clients_id'], 'categories_clients_name'=>$name]);
        return Redirect::route('clients.all');
    }
}
