<?php

namespace App\Http\Controllers;

use App\Models\CategoriesClients;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CategoryClientController extends Controller
{
    public function all()
    {
        $categoriesClients = CategoriesClients::all();
        return view('categoryClient/getcategoriesclients')->with('categoriesClients', $categoriesClients);

    }
    public function deleteCategory(CategoriesClients $categoryClient):RedirectResponse{
        $categoryClient->delete();
        return Redirect::route('categoriesclient.all');
    }
    public function deleteConfirmCategory(CategoriesClients $categoryClient){
        return view('/categoryClient/deleteCategoryClientConfirm')->with('categoryClient', $categoryClient);
    }
    public function create(Request $request):RedirectResponse
    {
        $validated = $request->validate([
            'name' => 'required|unique:categories|max:100',
            'descuento' => 'required'
        ]);

        DB::table('categories')->insert(['name' =>$validated['name'], 'descuento'=>$validated['descuento']]);
        return Redirect::route('categoriesclients.all');
    }
    public function edit(CategoriesClients $categoryClient)
    {
        return view('/categoryClient/editCategoryClient', compact('categoryClient'));
    }

    public function update(Request $request, CategoriesClients $categoriesClients): RedirectResponse
    {
        $validated = $request->validate([
            'name' => 'required|max:100|unique:categories,name,' . $categoriesClients->id,
            'descuento' => 'required|numeric|min:0',
        ]);

        Product::where('category_id', $categoriesClients->id)->update(['category_name' => $validated['name']]);


        $categoriesClients->update($validated);
        return Redirect::route('categoriesclients.all');
    }
}
