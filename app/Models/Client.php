<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'surname', 'email','postalcode', 'phonenumber', 'address', 'categories'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function categoriesClients()
    {
        return $this->belongsTo(CategoriesClients::class, 'categoriesClients_id');
    }

}
