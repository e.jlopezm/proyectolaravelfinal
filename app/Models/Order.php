<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['client_id', 'order_date', 'total_price'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity','price');
    }
    public static function generateCustomId($year)
    {
        $lastOrder = DB::table('orders')
            ->whereYear('order_date', $year)
            ->orderBy('order_date', 'desc')
            ->first();

        $lastId = $lastOrder ? intval(explode('/', $lastOrder->custom_id)[0]) : 0;
        $newId = $lastId + 1;

        return "{$year}/{$newId}";
    }
}
