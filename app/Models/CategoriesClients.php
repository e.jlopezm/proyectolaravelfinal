<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriesClients extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'descuento',
    ];

    public function client()
    {
        return $this->belongsToMany(Client::class)->withTimestamps();
    }
}
